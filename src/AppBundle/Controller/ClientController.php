<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Trip;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;




class ClientController extends Controller
{
    /**
     * @Route("/", name="client_home")
     */
    public function indexAction()
    {
            return $this->render('front/index.html.twig');
    }

    /**
     * @Route("/airports", name="client_airports")
     */
    public function airportAction()
    {
        $url = $this->container->getParameter("api_endpoint")['url'].$this->container->getParameter("api_endpoint")['airports'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $data = json_decode($response);
        if($data->status){
            return $this->render(':front/airports:index.html.twig', [
                'airports' => $data->data
            ]);
        }

    }

    /**
     * @Route("/trip/list", name="client_trip_list")
     *
     */
    public function tripListAction(Request $request)
    {
        $id = $request->get('id');
        $url = $this->container->getParameter("api_endpoint")['url'].$this->container->getParameter("api_endpoint")['trip']['list'].$id;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $data = json_decode($response);

        if($data->status){
            return $this->render(':front/trip:list.html.twig', [
                'airports' => $data->data
            ]);
        }

    }

    /**
     * @Route("/trip/add", name="client_trip_add")
     * @Method({"GET", "POST"})
     */
    public function tripAddAction(Request $request)
    {

        $trip = new Trip();
        $form = $this->createForm('AppBundle\Form\TripType', $trip);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $url = $this->container->getParameter("api_endpoint")['url'].$this->container->getParameter("api_endpoint")['trip']['add'];


            $data = [
                "trip_name" => $request->request->get('appbundle_trip')['name'],
                "flights" => [$request->request->get('appbundle_trip')['flight1'],$request->request->get('appbundle_trip')['flight2']]
            ];


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch,CURLOPT_POST, count($data));
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
            $response = curl_exec($ch);

            $data = json_decode($response);

            return $this->redirectToRoute('client_trip_list', array('id' => $data->ID));
        }

        return $this->render('front/trip/add.html.twig', array(
            'trip' => $trip,
            'form' => $form->createView(),
        ));


    }

    /**
     * @Route("/trip/remove", name="client_trip_remove")
     */
    public function tripRemoveAction(Request $request)
    {

        $trip = new Trip();
        $form = $this->createForm('AppBundle\Form\TripType', $trip);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $url = $this->container->getParameter("api_endpoint")['url'].$this->container->getParameter("api_endpoint")['trip']['update'];


            $data = [
                "trip_id" => 7,
                "trip_name" => $request->request->get('appbundle_trip')['name'],
                "flights" => [$request->request->get('appbundle_trip')['flight1'],$request->request->get('appbundle_trip')['flight2']]
            ];


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
            $response = curl_exec($ch);

            $response = json_decode($response);
            if(!$response->status) return new Response('Error!');

            return $this->redirectToRoute('client_trip_list', array('id' => $data['trip_id']));
        }

        return $this->render('front/trip/remove.html.twig', array(
            'trip' => $trip,
            'form' => $form->createView(),
        ));
    }


}
