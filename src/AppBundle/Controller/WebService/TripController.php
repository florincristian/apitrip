<?php

namespace AppBundle\Controller\WebService;

use AppBundle\Controller\mainWebServiceController;
use AppBundle\Entity\Airport;
use AppBundle\Entity\Trip;
use AppBundle\Entity\TripFlight;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationList;


/**
 * Trip controller.
 * @Route("/api/v1/trip")
 *
 */
class TripController extends mainWebServiceController
{
    /**
     * Lists all flights for a trip. You must specify a trip ID
     *
     *
     * ### Response when successful ###
     *
     *     {
     *       "status": true,
     *       "data": [
     *           {
     *               "code": "OTP",
     *               "name": "Henri Coanda International",
     *               "country_code": "RO"
     *           },
     *           {},{},...
     *       ]
     *       }
     *
     * ### Response when error ###
     *
     *     {
     *      "status": false,
     *      "data": [
     *          "Error 1",
     *          ...
     *      ]
     *     }
     *
     *
     *
     * @Route("/{id}", name="api_getTrip")
     * @Method("GET")
     * @ApiDoc(
     * section="Trip",
     * description="Lists all flights for a trip",
     * resource=true,
     * statusCodes={
     *         200="Returned when successful",
     *         404="Returned when error > trip not found",
     *         500="Returned when other error has occured"
     *     },
     *      requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Trip ID"}
     *  },
     * )
     */
    public function indexAction($id)
    {

        $em = $this->getManager();

        // get the trip
        $trip = $em->getRepository('AppBundle:Trip')->find($id);

        if (!$trip) {
            $errors_data = [
                'status' => false,
                'data' => sprintf('No trip found. Id: %s', $id)
            ];
            // return JsonResponse with error & 404 http code
            return new JsonResponse($errors_data, 404);
        }

        $tripflights = $trip->getTripflight();

        /** @var TripFlight $tf */
        foreach ($tripflights as $tf) {
            /** @var Airport $flight */
            $flight = $tf->getFlight();
            $data[] = [
                'code'=>$flight->getCode(),
                'name'=>$flight->getName(),
                'country_code'=>$flight->getCountryCode(),
            ];
        }

        $response['status']=true;
        $response['data']=$data;

        return new JsonResponse($response);
    }

    /**
     * Add flights to a trip. You must specify a trip name and an array with minimum 2 airport codes (flights).
     *
     * ### Response when successful ###
     *
     *     {
     *      "status": true,
     *      "data": "Trip successfully created",
     *      "ID": 324
     *     }
     *
     * ### Response when error ###
     *
     *     {
     *      "status": false,
     *      "data": [
     *          "Error 1",
     *          ...
     *      ]
     *     }
     *
     *
     * @Route("/", name="api_addTrip")
     * @Method("POST")
     * @ApiDoc(
     *      section="Trip",
     *      description="Create a trip",
     *      statusCodes={
     *         204="Returned when successful",
     *         500="Returned when error has occured"
     *     },
     *      parameters={
     *      {"name"="trip_name", "dataType"="string", "required"=true, "format"="\s+", "description"="Trip name"},
     *      {"name"="flights", "dataType"="array", "required"=true, "format"="\s+", "description"="Trip flights.  Contains airport CODES"},
     *  },
     * )
     */
    public function addTripAction(Request $request)
    {

        // get manager & validator
        $em = $this->getManager();
        $validator = $this->get('validator');

        //$m = $request->getMethod();
        //echo $m; exit;


        // get request parameters
        $req_trip_name = $request->request->get('trip_name');
        $req_flights = $request->request->get('flights');
        //echo (count($req_flights));exit;

        // create a new Trip, set name & add flight (airport)
        $trip = new Trip();
        $trip->setName($req_trip_name);

        $airportRepo = $em->getRepository('AppBundle:Airport');

        foreach ($req_flights as $i => $f) {
            // if airport code exists
            if ($flight = $airportRepo->findOneByCode($f)) {

                $tripflight = new TripFlight();
                $trip->addTripFlight($tripflight);
                $tripflight->setTrip($trip);
                $tripflight->setFlight($flight);
                $tripflight->setOrderno($i);

                $em->persist($tripflight);
                $em->persist($trip);
            }
        }

        // validate Trip entity
        $errors = $validator->validate($trip);

        // if error
        if (count($errors) > 0) {
            $errors_data = ['status' => false];
            /** @var ConstraintViolationList $er */
            foreach ($errors as $er) {
                $errors_data['data'][] = $er->getMessage();
            }
            // return JsonResponse with error & 500 http code
            return new JsonResponse($errors_data, 500);
        }

        try {

            $em->flush();

            // set response data
            $data = [
                'status' => true,
                'data' => 'Trip successfully created',
                'ID' => $trip->getId()
            ];

            // return JsonResponse with 204 http code
            return new JsonResponse($data);

        } catch (Exception $e) {
            $errors_data = [
                'status' => false,
                'data' => "Internal server error"
            ];

            // return JsonResponse with error & 500 http code
            return new JsonResponse($errors_data, 500);
        }

    }

    /**
     * Update a trip. You must specify the trip_id of an existing trip, a trip name and an array with minimum 2 airport codes (flights).
     *
     * Response when successful
     *
     *     {
     *      "status": true,
     *      "data": "Trip successfully updated"
     *     }
     *
     * Response when error
     *
     *     {
     *      "status": false,
     *      "data": [
     *          "Error 1",
     *          ...
     *      ]
     *     }
     *
     *
     * @Route("/", name="api_updateTrip")
     * @Method("PUT")
     * @ApiDoc(
     *     section="Trip",
     *     description="Update a trip",
     *     statusCodes={
     *         200="Returned when successful",
     *         404="Returned when error > trip not found",
     *         500="Returned when other error has occured"
     *     },
     *  parameters={
     *      {"name"="trip_id", "dataType"="integer", "required"=true, "format"="\d+", "description"="Trip id"},
     *      {"name"="trip_name", "dataType"="string", "required"=true, "format"="\s+", "description"="Trip name"},
     *      {"name"="flights", "dataType"="array", "required"=true, "format"="\s+", "description"="Trip flights.  Contains airport CODES"},
     *  },
     * )
     */
    public function updateTripAction(Request $request)
    {

        // get manager & validator
        $em = $this->getManager();
        $validator = $this->get('validator');

        // get request parameters
        $req_trip_id = $request->request->get('trip_id');
        $req_trip_name = $request->request->get('trip_name');
        $req_flights = $request->request->get('flights');


        // get trip instance
        $trip = $em->getRepository('AppBundle:Trip')->find($req_trip_id);

        // if trip with trip_id param not exist
        if (!$trip) {
            $errors_data = [
                'status' => false,
                'data' => sprintf('No trip found. Id: %s', $req_trip_id)
            ];
            // return JsonResponse with error & 404 http code
            return new JsonResponse($errors_data, 404);
        }

        // set new name for trip
        $trip->setName($req_trip_name);

        // remove all current tripflight records
        $tripflights = $trip->getTripflight();
        /** @var TripFlight $tf */
        foreach ($tripflights as $tf) {
            $em->remove($tf);
            $em->flush();
        }

        $airportRepo = $em->getRepository('AppBundle:Airport');

        // add flights in orders
        foreach ($req_flights as $i => $f) {
            // if airport code exists
            if ($flight = $airportRepo->findOneByCode($f)) {
                $tripflight = new TripFlight();
                $trip->addTripFlight($tripflight);
                $tripflight->setTrip($trip);
                $tripflight->setFlight($flight);
                $tripflight->setOrderno($i);
                $em->persist($tripflight);
                $em->persist($trip);
            }
        }

        // validate Trip entity
        $errors = $validator->validate($trip);

        // if validation error
        if (count($errors) > 0) {
            $errors_data = ['status' => false];
            /** @var ConstraintViolationList $er */
            foreach ($errors as $er) {
                $errors_data['data'][] = $er->getMessage();
            }
            // return JsonResponse with error & 500 http code
            return new JsonResponse($errors_data, 500);
        }

        try {
            $em->flush();

            // set response data
            $data = [
                'status' => true,
                'data' => 'Trip successfully updated'
            ];

            // return JsonResponse with 200 http code
            return new JsonResponse($data);

        } catch (Exception $e) {
            $errors_data = [
                'status' => false,
                'data' => "Internal server error"
            ];

            // return JsonResponse with error & 500 http code
            return new JsonResponse($errors_data, 500);
        }
    }
}
