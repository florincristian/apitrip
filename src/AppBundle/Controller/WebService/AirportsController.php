<?php

namespace AppBundle\Controller\WebService;

use AppBundle\Controller\mainWebServiceController;
use AppBundle\Entity\Airport;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Airports controller.
 *
 * @Route("/api/v1/airport")
 */
class AirportsController extends mainWebServiceController
{
    /**
     * Retrieves information about airports. Get the list of all airports in the world alphabetically ordered.
     *
     *
     * ### Response ###
     *
     *     {
     *      "status": true,
     *      "data": [
     *          {
     *           "code":"ZMH",
     *           "name":"108 Mile Ranch",
     *           "countryCode":"CA"
     *          },
     *          {},{},...
     *      ]
     *     }
     *

     *
     * @Route("/")
     * @Method("GET")
     * @ApiDoc(
     *  section="Airport",
     *  description="List all airports",
     * resource=true,

     * )
     */
    public function indexAction()
    {
        $this->setupForExecution();
        $airports = $this->getManager()->getRepository('AppBundle:Airport')->findAllOrderedByName();
        $response = [
            'status'=>true,
            'data'=>$airports
        ];
        return new JsonResponse($response);
    }
}
