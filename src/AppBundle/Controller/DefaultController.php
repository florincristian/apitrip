<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Airport;
use AppBundle\Entity\Trip;
use AppBundle\Entity\TripFlight;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Goutte\Client;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $codes = ['OTP','AAB','AAA'];
        $frepo = $this->getDoctrine()->getRepository('AppBundle:Airport')->findByCode($codes);

        dump($frepo); exit;


        $trip = new Trip();
        $trip->setName('Trip1');



        /**
         * @var  Airport $f
         */
        foreach($frepo as $i=>$f){
            $tripflight = new TripFlight();
            $trip->addTripFlight($tripflight);
            $tripflight->setTrip($trip);
            $tripflight->setFlight($f);
            $tripflight->setOrderno($i);

            $em->persist($tripflight);
            $em->persist($trip);

        }

        $em->flush();




        return new Response('ok');

    }

    /**
     * @Route("/apipost", name="apipost")
     */
    public function postAction()
    {

        $client = new Client();
        $data = [
            'name' => 'Trip1',
            'f'=> 'f1',
            'f[]'=> 'f2',
            'f[]'=> 'f3',
        ];

        $p = $client->request('POST', 'api/v1/', $data);
        //dump($p); exit;

        return new Response("ok");

    }
}
