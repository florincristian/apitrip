<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class mainWebServiceController extends Controller
{
    /**
     * @return EntityManager
     */
    function getManager()
    {
        //$clientAlias = $this->get('request')->get('client');
        //$endpoint = $this->getParameter('api_endpoint')['airports'];


        return $this->getDoctrine()->getManager();
    }

    protected function setupForExecution()
    {
        set_time_limit(0);
        ini_set('max_execution_time',0);
    }
}
