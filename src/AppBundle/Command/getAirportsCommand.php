<?php
namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class getAirportsCommand extends ContainerAwareCommand
{
protected function configure()
{
    $this
        // the name of the command (the part after "bin/console")
        ->setName('apitrip:get-airports')

        // the short description shown while running "php bin/console list"
        ->setDescription('Get and populate the airport table from https://iatacodes.org/api/v6/airports')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp("This command allows you to populate airports list from command line (or cron)")
    ;
}

protected function execute(InputInterface $input, OutputInterface $output)
{

    $iataService = $this->getContainer()->get('app.iata_codes');
    $iataService->populateAirport();

    $output->writeln('The airport table was updated!');
}
}