<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TripFlight
 *
 * @ORM\Table(name="trip_flight")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TripFlightRepository")
 */
class TripFlight
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Trip", inversedBy="tripflight")
     * @ORM\JoinColumn(name="trip_id", referencedColumnName="id")
     */
    private $trip;

    /**
     * @ORM\ManyToOne(targetEntity="Airport", inversedBy="tripflight")
     * @ORM\JoinColumn(name="flight_id", referencedColumnName="id")
     */
    private $flight;

    /**
     * @var int
     *
     * @ORM\Column(name="orderno", type="integer")
     */
    private $orderno;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get trip
     * @return Trip
     */
    public function getTrip()
    {
        return $this->trip;
    }

    /**
     * @param Trip $trip
     * @return TripFlight
     */
    public function setTrip(Trip $trip=null)
    {
        $this->trip = $trip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlight()
    {
        return $this->flight;
    }

    /**
     * @param Airport $flight
     * @return TripFlight
     */
    public function setFlight(Airport $flight=null)
    {
        $this->flight = $flight;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderno()
    {
        return $this->orderno;
    }

    /**
     * @param int $orderno
     */
    public function setOrderno($orderno)
    {
        $this->orderno = $orderno;
    }


}

