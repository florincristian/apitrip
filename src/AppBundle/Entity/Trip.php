<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trip
 *
 * @ORM\Table(name="trip")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TripRepository")
 */
class Trip
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message = "You must specify a trip name")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="TripFlight", mappedBy="trip")
     * @Assert\Count(
     *      min = "2",
     *      minMessage = "You must specify at least 2 flights",
     * )
     */
    private $tripflight;

    public function __construct()
    {
        $this->tripflight = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Trip
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getTripflight()
    {
        return $this->tripflight;
    }

    /**
     * Add tripflight
     *
     * @param TripFlight $tripFlight
     * @return Trip
     */
    public function addTripFlight(TripFlight $tripFlight)
    {
        $this->tripflight[] = $tripFlight;
        return $this;
    }

    /**
     * Remove tripflight
     *
     * @param TripFlight $tripFlight
     */
    public function removeTripFlight(TripFlight $tripFlight)
    {
        $this->tripflight->removeElement($tripFlight);
    }


}

