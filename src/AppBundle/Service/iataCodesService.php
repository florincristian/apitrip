<?php
namespace AppBundle\Service;

use AppBundle\Entity\Airport;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

class iataCodesService
{

    protected $em;
    protected $api_url;

    function __construct(EntityManager $em, $api_url)
    {
        $this->em = $em;
        $this->api_url = $api_url;
    }


    public function getAirports()
    {

        try {

            //get all airports via api iata
            $airports_api_request = file_get_contents($this->api_url);



            $airports_json = json_decode($airports_api_request);

            /*foreach($airports_json->response as $airport){
                echo $airport->code, " ", $airport->name, " ", $airport->country_code, "<br>";
            }*/

            return $airports_json->response;

        } catch (\Exception $e) {
            return new Response("Error! " . $e->getMessage());
        }
    }

    public function populateAirport()
    {

        try {
            set_time_limit(0);
            ini_set('max_execution_time', 0);
            $batchSize = 200; $i=0;
            if ($response = $this->getAirports()) {
                foreach($response as $ap) {
                    $i++;
                    //echo $ap->code,",",$ap->name,",",$ap->country_code."<br>";
                    $airport = new Airport();
                    $airport->setCode($ap->code);
                    $airport->setName($ap->name);
                    $airport->setCountryCode($ap->country_code);
                    $this->em->persist($airport);
                    if (($i % $batchSize) == 0) {
                        $this->em->flush();
                        $this->em->clear();
                    }

                    //print_r($airport);
                }
                $this->em->flush();
                $this->em->clear();
                return true;
            }
        } catch (\Exception $e) {
            return new Response("Error! " . $e->getMessage());
        }

    }

}
