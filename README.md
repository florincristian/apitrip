apitripbuilder
=============
This project is made with [symfony 3](https://symfony.com).

Project installation
-------------

To install you must have [Git](https://git-scm.com/) and [Composer](http://getcomposer.org/) installed.

Clone repository:

      git clone git@bitbucket.org:florincristian/apitrip.git


Install third-party code:

      composer install

After running `composer install` you must provide some parameters.

Setup database:

      php bin/console doctrine:schema:update -f

Populate database with airports using [IATA Codes API](https://iatacodes.org)

      php bin/console apitrip:get-airports

